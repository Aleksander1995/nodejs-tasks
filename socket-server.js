var server =  require ("socket.io")()
const events = require('events')
require('dotenv').config()

var MongoClient = require('mongodb').MongoClient,
  mongoUrl = 'mongodb://'+ process.env.DB_HOST + '/' + process.env.DB_NAME
var emitter = new events()
var mongodb;
var connect = false;

function connectToMongo() {
  MongoClient.connect(mongoUrl, {server: {auto_reconnect: true}})
  .then(db => {
    mongodb = db
    connect = true
    mongodb.collection('users').createIndex({id: 1}, {unique: true})
    .then(() => console.log('add index of user'))
    .catch((err) => console.log('Error message add user index: ' + err.message))
    mongodb.collection('posts').createIndex({id: 1}, {unique: true})
    .then(() => console.log('add index of post'))
    .catch((err) => console.log('Error message add post index: ' + err.message))
    console.log("connect to mongo")
  })
  .catch(err => {
    console.log("can not connect to mongodb: " + err.message)
    setTimeout(connectToMongo, 5000)
  })
}

connectToMongo()

function getNewPostId() {
  return new Promise((resolve, reject) => {
    mongodb.collection('posts')
    .find({}, {
      _id: false,
      id: true
    })
    .sort({id: -1})
    .limit(1)
    .toArray()
    .then( post => {
      if(post.length == 0) {
        return resolve(1)
      }
      return resolve(post[0].id + 1)
    })
    .catch(error => {
      reject(error)
    })
    setTimeout(() => {
      reject (new Error("db timeout"))
    }, process.env.DB_TIMEOUT)
  })
}

function getUserId(name) {
  return new Promise((resolve, reject) => {
    mongodb.collection("users")
    .find({name: name}, {
      _id: false,
      id: true
    })
    .toArray()
    .then(user => {
      if (user.length > 0) {
        return resolve({userId: user[0].id, new: false})
      }
      return mongodb.collection('users')
      .find({},{
        _id: false,
        id: true
      })
      .sort({id: -1})
      .limit(1)
      .toArray()
      .then(user => {
        if (user.length == 0) {
          return resolve({userId: 1, new: true})
        }
        return resolve({userId: (user[0].id + 1), new: true})
      })
    })
    .catch(error => {
      reject(error)
    })
    setTimeout(() => {
      reject(new Error("db timeout"))
    }, process.env.DB_TIMEOUT)
  })
}

server.on('connection', function (socket) {
  console.log('smd connected')
  socket.on("posts", function(cb){
    socketCallback("posts", cb)
  })

  socket.on("users", function(cb){
    socketCallback("users", cb)
  })
  socket.on('addPost', (newPost, cb) => {
    if(!connect) {
      cb({message: 'Нет соединения с базой'})
      return
    }
    Promise.all([getUserId(newPost.name), getNewPostId()])
    .then( ([UserId, postId]) => {
      console.log(UserId.userId)
      console.log(postId)
      newPost.userId = UserId.userId
      newPost.id = postId
      newPost.newUserId = UserId.new
      insertPostToDb(newPost).then(() => {
        console.log("add new Post here")
        var addPostResponse = {
          userId: UserId.userId,
          postId: postId
        }
        cb(null,addPostResponse)
        socket.broadcast.emit('addPost', newPost)
      })
      .catch(error => {
        cb({message: "Ошибка добавления в базу"})
      })
    })
    .catch(error => {
      cb({message: 'Ошибка при получении Id user или post'})
    })
  })
})
function insertPostToDb(newPost) {
  return new Promise((resolve, reject) => {
    var postChunk = {
      id: newPost.id,
      userId: newPost.userId,
      body: newPost.text
    }
    mongodb.collection('posts').insert(postChunk).then(() => {
      if (!newPost.newUserId) {
        return true
      }
      var userChunk = {
        id: newPost.userId,
        name: newPost.name
      }
      return mongodb.collection('users').insert(userChunk)
    })
    .then(() => resolve())
    .catch(error => {
      reject(error)
    })
    setTimeout(() => reject(new Error("db timeout insert")), process.env.DB_TIMEOUT)
  })
}

function socketCallback(event, cb){
  console.log(event)
  if (event != "users" && event != "posts"){
    console.log("Error - no collection exists")
    cb({message: 'Неправильный запрос'})
    return
  }
  if (emitter.listeners(event).length == 0){
    if (!connect) {
      console.log("db is not connected")
      cb({message: 'Нет подключения к базе'})
      return
    }
     new Promise((resolve, reject) => {
      setTimeout( () => {
        reject(new Error("Db is not connected"))
      }, process.env.DB_TIMEOUT)
      var col = mongodb.collection(event)
      col.find().toArray().then(result => {
        resolve(result)
      })
      .catch(err => {
        reject(err)
      })
    })
    .then(result => {
      emitter.emit(event, null, result)
    })
    .catch(err => {
      console.log("Error request: " + err.message)
      emitter.emit(event, {message: "Таймаут базы данных сработал"})
    })
  }
  emitter.once(event, (err, data) => {
    if(err == null) {
      console.log(event + " connection confirmed")
      cb(null, data)
    } else {
      console.log(event + " connection return error request")
      cb({message: err.message})
    }
  })
}

server.listen(process.env.WS_PORT)
